use crate::input::INPUT;

mod input;

fn main() {
    let mut game_ids: Vec<usize> = vec![];

    let mut total_cube_power = 0;

    for line in INPUT.split("\n") {
        let mut has_added = false;

        let mut min_red_cubes = 0;
        let mut min_green_cubes = 0;
        let mut min_blue_cubes = 0;

        let line = line.strip_prefix("Game ").unwrap();

        let pos = line.find(":").unwrap();
        let game_id: usize = line.chars().take(pos).collect::<String>().parse().unwrap();

        let rest: String = line.chars().skip(pos + 2).collect();

        for set in rest.split("; ") {
            for color in set.split(", ") {
                let (amount, color) = color.split_at(color.find(" ").unwrap());
                let color = color.trim();

                let amount: i32 = amount.parse().unwrap();

                match color {
                    "red" => {
                        if amount > 12 && !has_added {
                            has_added = true;
                        }

                        if amount > min_red_cubes {
                            min_red_cubes = amount;
                        }
                    }
                    "green" => {
                        if amount > 13 && !has_added {
                            has_added = true;
                        }

                        if amount > min_green_cubes {
                            min_green_cubes = amount;
                        }
                    }
                    "blue" => {
                        if amount > 14 && !has_added {
                            has_added = true;
                        }

                        if amount > min_blue_cubes {
                            min_blue_cubes = amount;
                        }
                    }

                    _ => { panic!("this should not happen.") }
                }
            }
        }

        if !has_added {
            game_ids.push(game_id);
        }

        let cube_power = min_red_cubes * min_green_cubes * min_blue_cubes;
        total_cube_power += cube_power;
    }

    let sum = game_ids.iter().cloned().reduce(|v1, v2| v1 + v2).unwrap();
    println!("Part 1: {sum}");
    println!("Part 2: {total_cube_power}");
}
