fn main() {
    let input = include_str!("input");

    let times: Vec<u64> = input.lines().nth(0).unwrap().strip_prefix("Time:").unwrap().split_whitespace().map(|v| v.parse().unwrap()).collect();
    let distances: Vec<u64> = input.lines().nth(1).unwrap().strip_prefix("Distance:").unwrap().split_whitespace().map(|v| v.parse().unwrap()).collect();

    let mut solution_counts: Vec<u64> = vec![];

    for i in 0..times.len() {
        let time = times.get(i).unwrap();
        let distance_record = distances.get(i).unwrap();

        let average_speed: f64 = *time as f64 / *distance_record as f64;

        let mut solution_count = 0;

        for button_hold_length in average_speed.ceil() as u64..*time {
            let time_left = time - button_hold_length;
            let distance = time_left * button_hold_length;

            if distance > *distance_record {
                solution_count += 1;
            }
        }

        solution_counts.push(solution_count);
    }

    let solution: u64 = solution_counts.iter().product();
    println!("Part 1: {solution}");
}
