mod input;

use input::INPUT;


fn main() {
    let mut results: Vec<i32> = vec![];

    let numbers = [
        "one",
        "two",
        "three",
        "four",
        "five",
        "six",
        "seven",
        "eight",
        "nine",
    ];

    for (i, line) in INPUT.split("\n").enumerate() {
        let mut first_num_index = -1;
        let mut last_num_index = -1;

        let mut first_num = -1;
        let mut last_num = -1;


        // look for digits (1, 2, 3)
        for (i, char) in line.chars().enumerate() {
            if char.is_ascii_digit() {
                if first_num_index == -1 {
                    first_num_index = i as i32;
                    first_num = char.to_string().parse().unwrap();
                }

                last_num_index = i as i32;
                last_num = char.to_string().parse().unwrap();
            }
        }

        // look for words (one, two three)
        for (i, number) in numbers.iter().enumerate() {
            let index = line.find(number);

            match index {
                None => {}
                Some(num_index) => {
                    if (num_index as i32) < first_num_index || first_num_index == -1 {
                        first_num_index = num_index as i32;
                        first_num = i as i32 + 1;
                    }
                }
            }

            let index = line.rfind(number);

            match index {
                None => {}
                Some(num_index) => {
                    if (num_index as i32) > last_num_index {
                        last_num_index = num_index as i32;
                        last_num = i as i32 + 1;
                    }
                }
            }
        }

        println!("Line {i}: {} - {}", first_num, last_num);

        results.push(first_num * 10 + last_num);
    }

    let total = results.iter().cloned().reduce(|v1, v2| {
        v1 + v2
    }).unwrap();

    println!("Part 1: {total}");
}
