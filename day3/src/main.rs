use std::collections::HashMap;

use grid::{grid, Grid};

use input::INPUT;

mod input;

fn main() {
    let mut grid: Grid<char> = grid![];

    let mut total: i32 = 0;

    let mut gear_numbers: HashMap<(usize, usize), Vec<i32>> = HashMap::new();

    for line in INPUT.split("\n") {
        if line.trim().is_empty() {
            continue;
        }
        grid.push_row(line.chars().collect());
    }

    for (i, row) in grid.iter_rows().enumerate() {
        let mut current_number = String::new();
        let mut current_number_start_index: Option<usize> = None;
        let mut current_number_end_index = 0;

        let mut iter = row.enumerate().peekable();
        while let Some((j, char))  = iter.next() {
            if char.is_ascii_digit() {
                current_number.push(*char);

                match current_number_start_index {
                    None => {
                        current_number_start_index = Some(j);
                    }
                    Some(_) => {}
                };
            }

            if !char.is_ascii_digit() || iter.peek().is_none() {

                if current_number_start_index.is_none() {
                    continue;
                }
                // dbg!(current_number_start_index.unwrap(), current_number_end_index);

                current_number_end_index = j - 1;


                let mut has_found = false;
                let mut has_inserted_current_number = false;
                for index in current_number_start_index.unwrap()..=current_number_end_index {

                    if has_found {
                        break;
                    }

                    let mut check_pos = |row: usize, col: usize| {
                        return match grid.get(row, col) {
                            None => {
                                false
                            }
                            Some(char) => {
                                if !char.is_ascii_digit() && char != &'.' {
                                    if !has_inserted_current_number && char == &'*' {

                                        if gear_numbers.get(&(row, col)).is_none() {
                                            gear_numbers.insert((row, col), vec![current_number.parse().unwrap()]);
                                        } else {
                                            gear_numbers.get_mut(&(row, col)).unwrap().push(current_number.parse().unwrap());
                                        }

                                        has_inserted_current_number = true;
                                    }

                                    return true;
                                }

                                false
                            }
                        };
                    };


                    if i != 0 {
                        if index != 0 {
                            has_found = if check_pos(i - 1, index - 1) { true } else { has_found };
                        }
                        has_found = if check_pos(i - 1, index) { true } else { has_found };
                        has_found = if check_pos(i - 1, index + 1) { true } else { has_found };
                    }

                    if index != 0 {
                        has_found = if check_pos(i, index - 1) { true } else { has_found };
                    }
                    has_found = if check_pos(i, index) { true } else { has_found };
                    has_found = if check_pos(i, index + 1) { true } else { has_found };

                    if index != 0 {
                        has_found = if check_pos(i + 1, index - 1) { true } else { has_found };
                    }
                    has_found = if check_pos(i + 1, index) { true } else { has_found };
                    has_found = if check_pos(i + 1, index + 1) { true } else { has_found };
                }

                if has_found {
                    total += current_number.parse::<i32>().unwrap();
                } else {
                    println!("{current_number}");
                }

                current_number_start_index = None;
                current_number = String::new();
            }
        }
    }

    let mut gear_total = 0;

    for (_k, v) in gear_numbers.iter() {

        if v.len() == 2 {
            gear_total += v.get(0).unwrap() * v.get(1).unwrap();
        }
    }

    println!("Part 1: {total}");
    println!("Part 2: {gear_total}");
}
