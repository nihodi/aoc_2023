use std::ops::Range;
use std::str::FromStr;


#[derive(Debug)]
struct Map {
    destination_range: Range<u64>,
    source_range: Range<u64>,
    lengths: u64,
}

impl FromStr for Map {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let nums: Vec<u64> = s.split_whitespace().map(|s| s.parse().unwrap()).collect();

        let destination_range_start = nums.first().unwrap();
        let source_range_start = nums.get(1).unwrap();
        let range_length = nums.last().unwrap();

        Ok(
            Map {
                destination_range: *destination_range_start..destination_range_start + range_length,
                source_range: *source_range_start..source_range_start + range_length,
                lengths: *range_length,
            }
        )
    }
}

fn main() {
    let input = include_str!("input");

    let seeds: Vec<u64> = input.
        lines()
        .collect::<Vec<&str>>()
        .first().unwrap()
        .chars()
        .skip(7)
        .collect::<String>()
        .split_whitespace()
        .map(|s| s.parse().unwrap())
        .collect();


    dbg!(&seeds);
    let maps: Vec<Vec<Map>> = input.split("\n\n").skip(1).map(|s| s.split('\n').skip(1).map(|s| s.parse().unwrap()).collect()).collect();

    dbg!(&maps);

    let mut values: Vec<u64> = seeds.clone();
    for map_types in maps {
        dbg!(&values);

        let mut has_matched = vec![];

        for _i in 0..seeds.len() {
            has_matched.push(false);
        }

        for map in map_types {
            let mut new_values = values.clone();

            for (i, value) in values.iter().enumerate() {
                if has_matched[i] {
                    continue;
                }

                if map.source_range.contains(value) {
                    new_values[i] = map.destination_range.start + value - map.source_range.start;

                    has_matched[i] = true;
                }
            }

            values = new_values;
        }
    }

    dbg!(&values);

    let min = values.iter().reduce(|x1, x2| if x1 < x2 { x1 } else { x2 }).unwrap();
    println!("Part 1: {}", min);
}
