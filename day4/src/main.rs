mod input;

use std::collections::HashMap;
use input::INPUT;

fn calculate_score(matching_numbers: u32) -> u32 {
    if matching_numbers == 0 {
        return 0;
    }
    let base: u32 = 2;
    return 1 * base.pow(matching_numbers - 1);
}

#[derive(Debug)]
struct ScratchCard {
    winning_numbers: Vec<i32>,
    drawn_numbers: Vec<i32>,
    id: u32,
}

impl ScratchCard {
    fn calculate_score(&self) -> u32 {
        let count = self.get_matching_drawn_numbers();
        calculate_score(count)
    }

    fn get_matching_drawn_numbers(&self) -> u32 {
        let mut count = 0;
        for num in &self.winning_numbers {
            if self.drawn_numbers.contains(num) {
                count += 1;
            }
        }
        count
    }
}

impl From<&str> for ScratchCard {
    fn from(value: &str) -> Self {
        let pos = value.find(":").unwrap();

        let id: u32 = value.chars().skip(5).take(pos - 5).collect::<String>().trim().parse().unwrap();

        let rest: String = value.chars().skip(pos + 2).collect();

        let bruh: Vec<&str> = rest.split("|").take(2).collect();

        let winning_numbers: Vec<i32> = bruh.get(0).unwrap().trim().split_whitespace().collect::<Vec<&str>>().iter().map(|v| v.parse().unwrap()).collect();
        let drawn_numbers: Vec<i32> = bruh.get(1).unwrap().trim().split_whitespace().collect::<Vec<&str>>().iter().map(|v| v.parse().unwrap()).collect();

        ScratchCard {
            winning_numbers,
            drawn_numbers,
            id,
        }
    }
}

fn main() {
    let mut cards: Vec<ScratchCard> = vec![];
    for line in INPUT.split("\n") {
        cards.push(line.into());
    }


    let total_score: u32 = cards.iter().map(|v| v.calculate_score()).sum();
    println!("Part 1: {total_score}");

    let mut part_two_cards: HashMap<usize, u32> = HashMap::new();

    for (i, card) in cards.iter().enumerate() {
        let option = part_two_cards.get(&i);

        let count = match option {
            None => 1,
            Some(c) => *c
        };

        let score = card.get_matching_drawn_numbers();

        for j in i + 1..i + score as usize + 1 {
            let previous = part_two_cards.insert(j, 1 * count + 1);
            match previous {
                None => {}
                Some(prev) => {
                    part_two_cards.insert(j, 1 * count + prev);
                }
            }
        }
    }

    let mut total = 0;
    for i in 0..cards.len() {
        let test = part_two_cards.get(&i);
        match test {
            None => {total += 1;}
            Some(count) => {total += count}
        }
    }

    println!("{total}");
}